# Создать (не программно) текстовый файл со следующим содержимым: 
# One — 1
# Two — 2
# Three — 3
# Four — 4
# Напишите программу, открывающую файл на чтение и считывающую построчно данные. 
# При этом английские числительные должны заменяться на русские. 
# Новый блок строк должен записываться в новый текстовый файл.
inputFile = '5.4.txt' #input('Input filename: ')
chislitelnie = {'1': 'Один', '2': 'Два', '3': 'Три', '4':'Четыре'}
with open(inputFile, 'r', encoding='utf-8') as f:
    data = f.read()
    Dict = dict((x.strip(), (y.strip()))
             for y, x in (element.split(' — ') 
             for element in data.split('\n')))
DictReplaced = {key: chislitelnie.get(key, Dict[key]) for key in Dict}
with open('output.txt', 'w', encoding='utf-8') as f:
    for i in DictReplaced:
        f.write(f'{DictReplaced[i]} — {i}\n')


