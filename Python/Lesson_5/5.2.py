# Создать текстовый файл (не программно), сохранить в нём несколько строк, 
# выполнить подсчёт строк и слов в каждой строке.
inputFile = input('What filename to read and calc words and strings: ')
countLines = 0
with open(inputFile, 'r') as f:
    for line in f:
        countLines += 1
        listOfWords = line.split()
        print(f'количество слов в строке {countLines} равно {len(listOfWords)}')
    print(f'количество строк {countLines}')
    
