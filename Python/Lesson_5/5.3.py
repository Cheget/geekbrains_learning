# Создать текстовый файл (не программно). 
# Построчно записать фамилии сотрудников и величину их окладов (не менее 10 строк). 
# Определить, кто из сотрудников имеет оклад менее 20 тысяч, вывести фамилии этих сотрудников. 
# Выполнить подсчёт средней величины дохода сотрудников.
# Пример файла:
# Иванов 23543.12
# Петров 13749.32
inputFile = input('Input filename: ')
with open(inputFile, 'r') as f:
    data = f.read()
    Dict = dict((x.strip(), float(y.strip()))
             for x, y in (element.split(' ') 
             for element in data.split('\n')))
print({x for (x,y) in Dict.items() if y > 20000.0})
print(sum(Dict.values())/len(list((Dict.values()))))

