# Создать (программно) текстовый файл, записать в него программно набор чисел, разделённых пробелами.
# Программа должна подсчитывать сумму чисел в файле и выводить её на экран.
with open('5.5.txt', 'w') as f:
    f.write(' '.join([str(x) for x in range(10, 100)]))
with open('5.5.txt', 'r') as f:
    data = f.read()
    myList = [int(x) for x in data.split(' ')]
print(sum(myList))
