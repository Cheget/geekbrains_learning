from itertools import count, cycle

start = int(input('Введите число с которого нужно начать последовательность: '))
stop = int(input('Введите размер последовательности: '))

listToRepeat = input('Введите список для повторения:')
times = int(input('Сколько раз повторить элементы списка: '))


def sequense (start, stop):
    iterator = count(start=start)
    print(list(next(iterator) for x in range(stop)))

def repeater (listToRepeat, times):
    iterator = cycle(listToRepeat)
    for x in iterator:
        if times != 0:
            print(x)
            times -=1
        else:
            break
sequense(start, stop)
repeater(listToRepeat, times)


