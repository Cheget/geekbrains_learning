DROP DATABASE vk;
CREATE DATABASE vk;

USE vk;

DROP TABLE IF EXISTS users;
-- Таблица пользователей
CREATE TABLE users (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT "Идентификатор строки", 
	email VARCHAR(100) NOT NULL UNIQUE COMMENT "Почта",
    phone VARCHAR(100) NOT NULL UNIQUE COMMENT "Телефон",
	created_at DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT "Время создания строки",  
	updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Время обновления строки"
);

DROP TABLE IF EXISTS profiles;
-- Таблица профилей
CREATE TABLE profiles (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT "Идентификатор строки", 
    user_id INT UNSIGNED UNIQUE NOT NULL COMMENT "Ссылка на пользователя", 
	first_name VARCHAR(100) NOT NULL COMMENT "Имя пользователя",
	last_name VARCHAR(100) NOT NULL COMMENT "Фамилия пользователя",
    birth_date DATE COMMENT "Дата рождения",    
    country VARCHAR(100) COMMENT "Страна проживания",
    city VARCHAR(100) COMMENT "Город проживания",
    `status` ENUM('ONLINE', 'OFFLINE', 'INACTIVE') COMMENT "Текущий статус",
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    -- ,PRIMARY KEY (`id`) -- вариант объявления PK
);

-- ALTER TABLE profiles ADD CONSTRAINT PRIMARY KEY (id); -- вариант объявления PK

-- Связываем поле "user_id" таблицы "profiles" с полем "id" таблицы "users" c помощью внешнего ключа
ALTER TABLE profiles ADD CONSTRAINT fk_profiles_user_id FOREIGN KEY (user_id) REFERENCES users(id);

DROP TABLE IF EXISTS messages;
-- Таблица сообщений
CREATE TABLE messages (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT "Идентификатор строки", 
	from_user_id INT UNSIGNED NOT NULL COMMENT "Ссылка на отправителя сообщения",
	to_user_id INT UNSIGNED NOT NULL COMMENT "Ссылка на получателя сообщения",
    message_header VARCHAR(255) COMMENT "Заголовок сообщения",
    message_body TEXT NOT NULL COMMENT "Текст сообщения",
    is_delivered BOOLEAN NOT NULL COMMENT "Признак доставки",
    was_edited BOOLEAN NOT NULL COMMENT "Признак правки заголовка или тела сообщения",
	created_at DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT "Время создания строки",
	updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Время обновления строки"
--     ,FOREIGN KEY (from_user_id) REFERENCES users(id), -- вариант объявления внешни ключей
--     FOREIGN KEY (to_user_id) REFERENCES users(id)
);

ALTER TABLE messages ADD CONSTRAINT fk_messages_from_user_id FOREIGN KEY (from_user_id) REFERENCES users(id);
ALTER TABLE messages ADD CONSTRAINT fk_messages_to_user_id FOREIGN KEY (to_user_id) REFERENCES users(id);

DROP TABLE IF EXISTS friendship;
-- Таблица дружбы
CREATE TABLE friendship (
    user_id INT UNSIGNED NOT NULL COMMENT "Ссылка на инициатора дружеских отношений",
    friend_id INT UNSIGNED NOT NULL COMMENT "Ссылка на получателя приглашения дружить",
    friendship_status ENUM('FRIENDSHIP', 'FOLLOWING', 'BLOCKED') COMMENT "Cтатус (текущее состояние) отношений",
	requested_at DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT "Время отправления приглашения дружить",
	confirmed_at DATETIME COMMENT "Время подтверждения приглашения",
	created_at DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT "Время создания строки",  
	updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Время обновления строки",  
	PRIMARY KEY (user_id, friend_id) COMMENT "Составной первичный ключ"
);

ALTER TABLE friendship ADD CONSTRAINT fk_friendship_user_id FOREIGN KEY (user_id) REFERENCES users(id);
ALTER TABLE friendship ADD CONSTRAINT fk_friendship_friend_id FOREIGN KEY (friend_id) REFERENCES users(id);

-- Таблица групп
CREATE TABLE communities (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT "Идентификатор сроки",
  name VARCHAR(150) NOT NULL UNIQUE COMMENT "Название группы",
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT "Время создания строки",  
  updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Время обновления строки"
) COMMENT "Группы";

-- Таблица связи пользователей и групп
CREATE TABLE communities_users (
  community_id INT UNSIGNED NOT NULL COMMENT "Ссылка на группу",
  user_id INT UNSIGNED NOT NULL COMMENT "Ссылка на пользователя",
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT "Время создания строки", 
  PRIMARY KEY (community_id, user_id) COMMENT "Составной первичный ключ"
) COMMENT "Участники групп, связь между пользователями и группами";

ALTER TABLE communities_users ADD CONSTRAINT fk_cu_user_id FOREIGN KEY (user_id) REFERENCES users(id);
ALTER TABLE communities_users ADD CONSTRAINT fk_cu_community_id FOREIGN KEY (community_id) REFERENCES communities(id);

-- Таблица постов
CREATE TABLE posts (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT "Идентификатор сроки",
	created_at DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT "Время создания строки",
	updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Время обновления строки",
	post_text BLOB NOT NULL,
	was_edited BOOLEAN NOT NULL COMMENT "Признак правки заголовка или тела поста"
) COMMENT "Посты";

-- Таблица связи пользователей и постов
CREATE TABLE user_posts (
	post_id INT UNSIGNED NOT NULL COMMENT "Ссылка на пост",
	user_id INT UNSIGNED NOT NULL COMMENT "Ссылка на пользователя",
	created_at DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT "Время создания строки",
	PRIMARY KEY (post_id, user_id) COMMENT "Составной первичный ключ"
) COMMENT "Пост пользователя";

ALTER TABLE user_posts ADD CONSTRAINT fk_pu_user_id FOREIGN KEY (user_id) REFERENCES users(id);
ALTER TABLE user_posts ADD CONSTRAINT fk_pu_posts_id FOREIGN KEY (post_id) REFERENCES posts(id);

-- Таблица медиафайлов
CREATE TABLE media_files (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT "Идентификатор сроки",
	upload_at DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT "Время загрузки медиа файла",
	content BLOB NOT NULL,
	likes_count INT DEFAULT NULL
) COMMENT "Медиафайлы";

-- Таблица связи постов и медиа в них
CREATE TABLE post_media (
	media_id INT UNSIGNED NOT NULL COMMENT "Ссылка на медиа файл",
	post_id INT UNSIGNED NOT NULL COMMENT "Ссылка на пост",
	PRIMARY KEY (post_id, media_id) COMMENT "Составной первичный ключ"
) COMMENT "Связь между постом и медиафайлами";

ALTER TABLE post_media ADD CONSTRAINT fk_pm_media_id FOREIGN KEY (media_id) REFERENCES media_files(id);
ALTER TABLE post_media ADD CONSTRAINT fk_pm_posts_id FOREIGN KEY (post_id) REFERENCES posts(id);

-- Таблица лайков постов
CREATE TABLE likes_post (
	user_id INT UNSIGNED UNIQUE NOT NULL COMMENT "Ссылка на пользователя",
	like_date DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT "Время создания строки",
	post_id INT UNSIGNED NOT NULL COMMENT "Ссылка на пост",
	PRIMARY KEY (post_id, user_id) COMMENT "Составной первичный ключ"
) COMMENT "Лайки постов";

ALTER TABLE likes_post ADD CONSTRAINT fk_lp_user_id FOREIGN KEY (user_id) REFERENCES users(id);
ALTER TABLE likes_post ADD CONSTRAINT fk_lp_posts_id FOREIGN KEY (post_id) REFERENCES posts(id);

-- Таблица лайков пользователей
CREATE TABLE likes_user (
	user_id INT UNSIGNED UNIQUE NOT NULL COMMENT "Ссылка на пользователя поставившего лайк",
	user_like_id INT UNSIGNED UNIQUE NOT NULL COMMENT "Ссылка полюбившегося пользователя", 
	like_date DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT "Время создания строки",
	PRIMARY KEY (user_id, user_like_id) COMMENT "Составной первичный ключ"
	
) COMMENT "Лайки пользователей";

ALTER TABLE likes_user ADD CONSTRAINT fk_lu_user_id FOREIGN KEY (user_id) REFERENCES users(id);
ALTER TABLE likes_user ADD CONSTRAINT fk_lu_user_like_id FOREIGN KEY (user_like_id) REFERENCES users(id);

-- Таблица лайков медиафайлов
CREATE TABLE likes_media (
	user_id INT UNSIGNED UNIQUE NOT NULL COMMENT "Ссылка на пользователя",
	media_id INT UNSIGNED NOT NULL COMMENT "Ссылка на медиа файл",
	like_date DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT "Время создания строки",
	PRIMARY KEY (media_id, user_id) COMMENT "Составной первичный ключ"
) COMMENT "Лайки медиафайлов";

ALTER TABLE likes_media ADD CONSTRAINT fk_lm_media_id FOREIGN KEY (media_id) REFERENCES media_files(id);
ALTER TABLE likes_media ADD CONSTRAINT fk_lm_user_id FOREIGN KEY (user_id) REFERENCES users(id);

select * from media_files mf ;

